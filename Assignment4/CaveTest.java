import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CaveTest {
	
	private Cave testCave0;
	private Cave testCave1;
	private Cave testCave2;
	private Cave testCave3;
	
	@Before
	public void setUp() throws Exception {
		testCave0 = new Cave(1, 1);
		testCave1 = new Cave(10, 10);
		testCave2 = new Cave(5, 5);
		testCave3 = new Cave(20, 20);
	}
	
	@After
	public void tearDown() throws Exception {
		testCave0 = null;
		testCave1 = null;
		testCave2 = null;
		testCave3 = null;
	}

	@Test
	public void test_getRow() {
		assertEquals(1, testCave0.getRow());
		assertEquals(10, testCave1.getRow());
		assertEquals(5, testCave2.getRow());
		assertEquals(20, testCave3.getRow());
	}

	@Test
	public void test_getCol() {
		assertEquals(1, testCave0.getRow());
		assertEquals(10, testCave1.getRow());
		assertEquals(5, testCave2.getRow());
		assertEquals(20, testCave3.getRow());
	}
	
	@Test
	public void test_marked() {
		testCave0.setMarked(false);
		assertFalse(testCave0.isMarked());
		
		testCave0.setMarked(true);
		assertTrue(testCave0.isMarked());	
	}
	
	@Test
	public void test_blocked() {
		testCave0.makeBlocked();
		assertTrue(testCave0.isBlocked());
		
		testCave0.makeOpen();
		assertFalse(testCave0.isBlocked());
		testCave0.makeOpen();
		assertFalse(testCave0.isBlocked());
		testCave0.makeTeleport();
		assertFalse(testCave0.isBlocked());
	}
	
	@Test
	public void test_occupied() {
		testCave0.setOccupied(false);
		assertFalse(testCave0.isOccupied());
		
		testCave0.setOccupied(true);
		assertTrue(testCave0.isOccupied());
	}

	@Test
	public void test_open() {
		testCave0.makeOpen();
		assertTrue(testCave0.isOpen());
		
		testCave0.makePit();
		assertFalse(testCave0.isOpen());
		testCave0.makeBlocked();
		assertFalse(testCave0.isOpen());
		testCave0.makeTeleport();
		assertFalse(testCave0.isOpen());
	}
	
	@Test
	public void test_pit() {
		testCave0.makePit();
		assertTrue(testCave0.isPit());
		
		testCave0.makeOpen();
		assertFalse(testCave0.isPit());
		testCave0.makeBlocked();
		assertFalse(testCave0.isPit());
		testCave0.makeTeleport();
		assertFalse(testCave0.isPit());
	}
	
	@Test
	public void test_teleport() {
		testCave0.makeTeleport();
		assertTrue(testCave0.isTeleport());
		
		testCave0.makeBlocked();
		assertFalse(testCave0.isTeleport());
		testCave0.makeOpen();
		assertFalse(testCave0.isTeleport());
		testCave0.makePit();
		assertFalse(testCave0.isTeleport());
	}
}