import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class BoardTest {
	
	private Board board;
	private final int ROWS = 10;
	private final int COLS = 10;
	
	@Before
	public void setUp() throws Exception {
		board = new Board(ROWS,COLS);
	}

	@After
	public void tearDown() throws Exception {
		board = null;
	}
	
	@Test
	public void test_getCave() {
		assertTrue(getCaveHelper(board.getCave(5, 5)));
		assertTrue(getCaveHelper(board.getCave(1, 1)));
		assertTrue(getCaveHelper(board.getCave(9, 1)));
	}

	private boolean getCaveHelper(Cave c) {
		if (c.isBlocked() || c.isOpen() || c.isPit() || 
				c.isTeleport()) {
			return true;
		}
		return false;
	}
	
	@Test
	public void test_getUnoccupiedOpenLocation() {
		for (int i = 0; i < 100; i++) {
			Cave testCave = board.getUnoccupiedOpenLocation();
			assertTrue(testCave.isOpen());
			assertFalse(testCave.isOccupied());
		}
	}
	
	@Test
	public void test_ok() {
		for (int y = 0; y < ROW; y++) {
			for (int x = 0; x < COL; x++) {
				assertTrue(board.ok(x, y));
			}
		}
		
		assertFalse(board.ok(0, -1));
		assertFalse(board.ok(0, -5));
		assertFalse(board.ok(1, 11));
		assertFalse(board.ok(7, -1));
		assertFalse(board.ok(23454524, 5));
		assertFalse(board.ok(111111, 234234));
	}
}
