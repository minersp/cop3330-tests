import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class MinerTest {
	
	Miner jovie;
	Board testBoard;
	
	private final int COL = 10;
	private final int ROW = 10;
	
	
	private final String NAME = "<put name here>";
	private final String MOD = "<put modification description here>";

	@Before
	public void setUp() throws Exception {
		testBoard = new Board(ROW, COL);
		jovie = new Miner(testBoard.getCave(0,0));
	}

	@After
	public void tearDown() throws Exception {
		jovie = null;
		testBoard = null;
	}

	@Test
	public void test_getName() {
		assertEquals(NAME, jovie.getName());
	}
	
	@Test
	public void test_modifyCave() {
		Cave testCave = testBoard.getCave(0, 1);
		testCave.makeOpen();
		assertFalse(jovie.modifyCave(testCave));
		assertFalse(testCave.isBlocked());
		
		testCave.makeBlocked();
		assertTrue(jovie.modifyCave(testCave));
		assertTrue(testCave.isOpen());
	}
	
	@Test
	public void test_describeModification() {
		assertEquals(MOD, jovie.describeModification());
	}

	@Test
	public void test_move() {
		Cave testCave = testBoard.getCave(0,1);
		testCave.makeBlocked();
		assertTrue(jovie.move(testCave));
		
		testCave.makeOpen();
		assertTrue(jovie.move(testCave));
		
		testCave.makePit();
		assertTrue(jovie.move(testCave));
		
		testCave.makeTeleport();
		assertTrue(jovie.move(testCave));
	}
}
