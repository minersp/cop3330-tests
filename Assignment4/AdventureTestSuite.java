import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ AdventurerTest.class, BoardTest.class, CaveTest.class,
		FillerTest.class, MinerTest.class })
public class AdventureTestSuite {

}
