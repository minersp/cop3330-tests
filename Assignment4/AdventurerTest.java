import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AdventurerTest {
	
	Adventurer buddy;
	Board testBoard;
	
	private final int COL = 10;
	private final int ROW = 10;
	
	private final String NAME = "<put name here>";
	private final String MOD = "<put modification description here>";
	
	@Before
	public void setUp() throws Exception {
		testBoard = new Board(ROW, COL);
		buddy = new Adventurer(testBoard.getCave(0,0));
	}
	
	@After
	public void tearDown() throws Exception {
		buddy = null;
		testBoard = null;
	}
	
	@Test
	public void test_getName() {
		assertEquals(NAME, buddy.getName());
	}
	
	@Test
	public void test_modifyCave() {
		Cave testCave = testBoard.getCave(0, 1);
		testCave.makeOpen();
		assertFalse(buddy.modifyCave(testCave));
		assertFalse(testCave.isMarked());
		
		testCave.makeTeleport();
		assertTrue(buddy.modifyCave(testCave));
		assertTrue(testCave.isMarked());
	}
	
	@Test
	public void test_describeModification() {
		assertEquals(MOD, buddy.describeModification());
	}

	@Test
	public void test_move() {
		Cave testCave = testBoard.getCave(0,1);
		testCave.makeBlocked();
		assertFalse(buddy.move(testCave));
		
		testCave.makeOpen();
		assertTrue(buddy.move(testCave));
		
		testCave.makePit();
		assertTrue(buddy.move(testCave));
		
		testCave.makeTeleport();
		assertTrue(buddy.move(testCave));
	}
}
