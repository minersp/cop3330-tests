import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class FillerTest {
	
	Filler walter;
	Board testBoard;
	
	private final int COL = 10;
	private final int ROW = 10;
	
	
	private final String NAME = "<put name here>";
	private final String MOD = "<put modification description here>";

	@Before
	public void setUp() throws Exception {
		testBoard = new Board(ROW, COL);
		walter = new Filler(testBoard.getCave(0,0));
	}

	@After
	public void tearDown() throws Exception {
		walter = null;
		testBoard = null;
	}

	@Test
	public void test_getName() {
		assertEquals(NAME, walter.getName());
	}
	
	@Test
	public void test_modifyCave() {
		Cave testCave = testBoard.getCave(0, 1);
		testCave.makeOpen();
		assertFalse(walter.modifyCave(testCave));
		assertFalse(testCave.isPit());
		
		testCave.makePit();
		assertTrue(walter.modifyCave(testCave));
		assertTrue(testCave.isOpen());
	}
	
	@Test
	public void test_describeModification() {
		assertEquals(MOD, walter.describeModification());
	}

	@Test
	public void test_move() {
		Cave testCave = testBoard.getCave(0,1);
		testCave.makeBlocked();
		assertFalse(walter.move(testCave));
		
		testCave.makeOpen();
		assertTrue(walter.move(testCave));
		
		testCave.makePit();
		assertTrue(walter.move(testCave));
		
		testCave.makeTeleport();
		assertTrue(walter.move(testCave));
	}
}
